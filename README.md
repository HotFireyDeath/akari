# Akari
Welcome to Akari, an open-source and free to use music streaming server + client. Akari makes it easy for you to enjoy your music files, however you'd like. What are you waiting for? Get started today.

*Note: Akari is not completely finished yet. It works in a practical condition, but you may find some features missing. Please refer to the feature implementation list below.*

##### Features
- [x] Multi-user support
- [x] Login/register wizard
- [x] Private server option (no registration, private use only)
- [x] Fresh theme and app icon
- [x] Upload songs
- [x] Play, pause, skip, shuffle, and loop songs (all basic player controls)
- [x] Offline compatible (PWA supported) *(browser limited)*
- [x] Download music for offline *(browser limited)*
- [x] Queue system (drag-and-drop interface)
- [x] Delete songs, uncache songs, add song to queue, and remove song from queue options.
- [ ] Edit song tags (MP3)
- [ ] Playlists
- [ ] Social features (user listening to, chat, music sharing, etc.)

#### How to install
Currently, Akari is not finished. Therefore it isn't in a ready package for anyone to easily install, and could be rough around the edges. If you're familiar with Node.js, download the package and run package.json to install all dependencies. Then, simply run index.js, the client (starting point) is available on localhost:port (default 8080) on your local machine or hosting service.