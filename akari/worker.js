var CACHE_NAME = 'akari-v1';
var cacheUrls = [
    "https://code.jquery.com/jquery-3.3.1.min.js",
    "https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js",
    "https://bootswatch.com/4/minty/bootstrap.min.css",
    "https://cdnjs.cloudflare.com/ajax/libs/localforage/1.7.3/localforage.min.js",
    "https://gitcdn.xyz/repo/CodeSeven/toastr/master/build/toastr.min.css",
    "https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.js",
    "https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js",
    "https://cdnjs.cloudflare.com/ajax/libs/howler/2.0.15/howler.min.js",
    "https://cdnjs.cloudflare.com/ajax/libs/howler/2.0.15/howler.core.min.js",
    "https://cdn.jsdelivr.net/gh/dankogai/js-base64@2.4.9/base64.min.js",
    "https://cdnjs.cloudflare.com/ajax/libs/Sortable/1.6.0/Sortable.min.js",
    "https://use.fontawesome.com/releases/v5.5.0/css/all.css",
    "images/login.jpg",
    "images/settingsIcon.png",
    "images/android-icon-192x192.png"
];
self.addEventListener('install', function (event) {
    cacheUrls.forEach(function (item) {
        var req = new Request(item, {mode: 'no-cors'});
        fetch(req).then(function (res) {
            caches.open(CACHE_NAME).then(function (cache) {
                return cache.put(req, res);
            });
        });
    });
});
self.addEventListener('fetch', function (event) {
    event.respondWith(
        caches.open(CACHE_NAME).then(function(cache) {
            return cache.match(event.request).then(function(response) {
                if (event.request.url.indexOf('/api/') === -1) {
                    var fetchPromise = fetch(event.request).then(function (networkResponse) {
                        cache.put(event.request, networkResponse.clone());
                        return networkResponse;
                    });
                    return response || fetchPromise;
                }
                else{
                    return fetch(event.request);
                }
            })
        })
    );
});