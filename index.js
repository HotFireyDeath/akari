
/*
    This is the Configuration for Akari.
    Please change these settings if you would like. They have been set to defaults.
 */
var AkariConfig = {
    // Set to true to enable public registration, or false to disable public registrations.
    registration: true
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Dependencies
var fs = require('fs');
var crypto = require('crypto');

var express = require('express');
var app = express();
var port = 8080;

var multer  = require('multer');
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, __dirname + '/db/uploads')
    },
    filename: function (req, file, cb){
        var extension = file.originalname.split('.')[1];
        cb(null, crypto.randomBytes(32).toString('base64').replace(/[/\\?%*:|"<>]/g, '') + '.' + extension);
    }
});
var upload = multer({ storage: storage });

var Base64 = require('js-base64').Base64;

const NodeID3 = require('node-id3');

var mime = require('mime-types');

// UploadHolding holds uploaded files as a cache while client is sending auth data.
var UploadHolding = {};

var Users = {
    /**
     * Creates a user.
     * @param username
     * @param password
     * @returns {string}
     */
    createUser: function(username, password){
        if ((this.exists(username)) || (password.length < 5) || (username.length < 3)){
            return "spec_not_met";
        }
        var salt = crypto.randomBytes(128).toString('base64');
        var iterations = 10000;
        var hashBytes = 32;
        var hash = crypto.pbkdf2Sync(password, salt, iterations, hashBytes, 'sha512');
        this.updateUserData(username, JSON.stringify({
            pass: {
                hash: hash,
                salt: salt,
                iterations: iterations,
                hashBytes: hashBytes
            },
            playlists: {},
            songs: {},
            friends: [],
            shared: {}, //shared inbox
            settings: {
                hideActivity: false
            }
        }));
        return "";
    },
    /**
     * Updates a user data with obj.
     * @param username String
     * @param obj Object that must be JSON stringifyable.
     */
    updateUserData: function(username, obj){
        fs.writeFileSync(__dirname + '/db/' + username + '.json', JSON.stringify(obj));
    },
    /**
     * Get a user's data.
     * @param username
     * @returns {any}
     */
    getUserData: function(username){
        var read = JSON.parse(fs.readFileSync(__dirname + '/db/' + username + '.json', 'utf8'));
        if ((typeof read) === 'string'){
            return JSON.parse(read);
        }
        else{
            return read;
        }
    },
    /**
     * Logs in a user. Returns a hash.
     * @param username
     * @param password
     * @returns {string}
     */
    loginUser: function(username, password){
        if (!this.exists(username)) {
            return "invalid_pswd";
        }
        var data = this.getUserData(username);
        var salt = data['pass']['salt'];
        var iterations = data['pass']['iterations'];
        var hashBytes = data['pass']['hashBytes'];
        var hash = crypto.pbkdf2Sync(password, salt, iterations, hashBytes, 'sha512').join('');
        var check = data['pass']['hash']['data'].join('');
        if (hash === check){
            // Create a new hash in the user db and return it.
            if ('authToken' in data['pass']){
                return data['pass']['authToken'];
            }
            else{
                data['pass']['authToken'] = crypto.randomBytes(256).toString('hex');
                this.updateUserData(username, data);
                return data['pass']['authToken'];
            }
        }
        else{
            return "invalid_pswd";
        }
    },
    /**
     * Checks if a user exists given their username.
     * @param username
     * @returns {boolean}
     */
    exists: function(username){
        return fs.existsSync(__dirname + '/db/' + username + '.json');
    },
    /**
     * Verifies that the token and username are valid.
     * @param username
     * @param token
     * @returns {boolean}
     */
    verifyToken: function(username, token){
        if (!this.exists(username)){
            return false;
        }
        else{
            // User exists...
            var read = this.getUserData(username);
            return token === read['pass']['authToken'];
        }
    },
    /**
     * Fetches basic information about friend's status.
     * @param username
     */
    friendFetch: function(username){
        if (!this.exists(username)){
            return false;
        }
        else{
            // TO-DO: Fetch friend data.
        }
    }
};

// Express routers.
app.enable('strict routing');
app.all('/akari', function(req, res) { res.redirect('/akari/'); });

app.get('/akari/images/:file', function(req, res){
    res.sendFile(__dirname + '/akari/images/' + req.params['file']);
});
app.get('/akari/manifest.json', function(req, res){
    res.sendFile(__dirname + '/akari/manifest.json');
});
app.get('/akari/worker.js', function(req, res){
    res.sendFile(__dirname + '/akari/worker.js');
});
app.get('/akari/index.html', function(req, res){
    res.sendFile(__dirname + '/akari/index.html');
});
app.get('/akari/', function(req, res){
    res.sendFile(__dirname + '/akari/index.html');
});

app.post('/akari/api/uploadMusic', upload.array('uploadModal_files'), function(req, res){
    var files = req.files;
    var id = encodeURIComponent(crypto.randomBytes(32).toString('base64'));
    UploadHolding[id] = files;
    res.redirect('/akari#uploadBuffer:' + id);
});

///// api router /////
app.get('/akari/api/:requestType/:requestData/', function(req, res){
    var params = req.params;
    function send(text){
        res.send(text);
    }
    var data = JSON.parse(Base64.decode(params['requestData']));
    switch(params['requestType']){
        case 'verifyToken':
            if (Users.verifyToken(data['username'], data['token'])){
                send('valid');
            }
            else{
                send('invalid');
            }
            break;
        case 'loginUser':
            fs.readdir(__dirname + '/db/', function(err, files){
                var response;
                if (files.length === 1){
                    response = Users.createUser(data[0], data[1]);
                    send(JSON.stringify({
                        status: response,
                        firstUser: ''
                    }));
                }
                else{
                    response = Users.loginUser(data[0], data[1]);
                    send(JSON.stringify({
                        status: response
                    }));
                }
            });
            break;
        case 'fetchUserData':
            if (!Users.exists(data['username'])){
                send(JSON.stringify({
                    status: 'user_not_exist'
                }));
            }
            else{
                if (Users.verifyToken(data['username'], data['token'])){
                    // Now that verifications are done, get the user's data and send it back.
                    var userData = Users.getUserData(data['username']);
                    delete userData['pass'];
                    // TO-DO: Fetch friends (data['friends'] array).
                    send(JSON.stringify({
                        status: 'success',
                        data: userData
                    }));
                }
                else{
                    send(JSON.stringify({
                        status: 'token_verification_fail'
                    }));
                }
            }
            break;
        case 'registerUser':
            if (AkariConfig.registration) {
                var username = data[0];
                var password = data[1];
                send(JSON.stringify({
                    status: Users.createUser(username, password)
                }));
            }
            else{
                send(JSON.stringify({
                    status: 'registration_disabled'
                }));
            }
            break;
        case 'uploadBuffer':
            // TO-DO: Check file extensions, make sure that files are music files.
            var robj = {};
            if (!Users.exists(data['username'])){
                robj['status'] = 'user_not_exist'
            }
            else{
                if (Users.verifyToken(data['username'], data['token'])){
                    var udb = Users.getUserData(data['username']);
                    if (data['buffer'] in UploadHolding){
                        var listOfFailures = [];
                        var validExtensions = [
                            'mp3','mpeg','ogg','wav','webm','weba','flac','caf','aac','m4a','mp4','dolby','oga'
                        ];
                        UploadHolding[data['buffer']].forEach(function(file){
                            var ext = file.originalname.split('.').pop();
                            if (validExtensions.indexOf(ext) !== -1) {
                                udb['songs'][file.originalname.replace('.' + ext, '') + crypto.randomBytes(8).toString('base64').replace(/[/\\?%*:|"<>]/g, '')] = file.path;
                            }
                            else{
                                fs.unlinkSync(file.path);
                                listOfFailures.push(file.originalname);
                            }
                        });
                        Users.updateUserData(data['username'], udb);
                        delete UploadHolding[data['buffer']];
                        if (listOfFailures.length === 0) {
                            robj['status'] = 'uploaded_success';
                        }
                        else{
                            robj['status'] = 'failures_detected';
                            robj['fails'] = listOfFailures;
                        }
                    }
                    else{
                        robj['status'] = 'invalid_buffer';
                    }
                }
                else{
                    robj['status'] = 'token_verification_fail'
                }
            }
            send(JSON.stringify(robj));
            break;
        case 'fetchSong':
            if (Users.exists(data['username'])){
                if (Users.verifyToken(data['username'], data['token'])){
                    var song = data['song'];
                    var d = Users.getUserData(data['username']);
                    if (song in d['songs']){
                        var path = d['songs'][song];
                        NodeID3.read(path, function(err, tags){
                            if (err) throw err;
                            tags['originalidentifier'] = song;
                            if ('image' in tags) {
                                tags['image']['imageBuffer'] = tags['image']['imageBuffer'].toString('base64');
                            }
                            else{
                                tags['image'] = {
                                    imageBuffer: 'iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAPFBMVEXT09PZ2dlvb29kZGRYWFhNTU1DQ0NLS0siIiJ0dHTg4OBCQkI4ODhSUlJsbGxeXl4GBgY0NDQtLS0oKCiUYzlNAAABZUlEQVR4nO3dOVLDUABEQXmV5QUDvv9dcUBCEeAi66/XN3j5VM30/nh8flyvl+NlPjztv913P2z/7W33p/v+BbfDC+bjL6dpPm9Gdl6mw2Ya2Wa7gsLb8IX7Cm0V+ir0Veir0Feh71l4H75wV6GtQl+Fvgp9Ffoq9FXoq9BXoa9CX4W+Cn0V+ir0Veir0LeKwm2Ftgp9Ffoq9FXoq9BXoa9CX4W+Cn0V+ir0Veir0Fehr0Jfhb4KfRX6VlH4Nnzh+IuhCnEV+ir0Veir0Fehr0Jfhb4KfRX6KvRV6KvQV6GvQl+Fvgp9Ffoq9FXoW8XPTIW4Cn0V+ir0Veir0Fehr0Jfhb4KfRX6KvRV6KvQV6GvQl+Fvgp9z8JDhbYKfRX6KvRV6KvQV6GvQl+Fvgp9Ffoq9FXoq9BXoa9CX4W+Cn3Pwnn4wmOFtgp9Ffoq9FXoq9BXoa9C3zoKz5uRnZfptGxHtly/APntDl+tpvv3AAAAAElFTkSuQmCC',
                                    type: 'png'
                                };
                            }
                            send(JSON.stringify(tags));
                        });
                    }
                    else{
                        send('song_key_not_exist');
                    }
                }
                else{
                    send('token_verification_fail');
                }
            }
            else{
                send('user_not_exist');
            }
            break;
        case 'fetchSongBuffer':
            if (Users.exists(data['username'])){
                if (Users.verifyToken(data['username'], data['token'])){
                    var ret = {};
                    var k = Users.getUserData(data['username']);
                    var id = data['song'];
                    if (data['song'] in k['songs']) {
                        var loc = k['songs'][data['song']];
                        fs.readFile(loc, function(err, data){
                            ret['songBuffer'] = data.toString('base64');
                            ret['songMime'] = mime.lookup(loc);
                            ret['id'] = id;
                            send(JSON.stringify(ret));
                        });
                    }
                    else{
                        send('song_key_not_exist');
                    }
                }
                else{
                    send('token_verification_fail');
                }
            }
            else{
                send('user_not_exist');
            }
            break;
        case 'deleteSong':
            if (Users.exists(data['username'])){
                if (Users.verifyToken(data['username'], data['token'])){
                    var user = Users.getUserData(data['username']);
                    fs.unlink(user.songs[data['id']], function(){
                        delete user.songs[data['id']];
                        Users.updateUserData(data['username'], user);
                        send('done');
                    });
                }
                else{
                    send('token_verification_fail');
                }
            }
            else{
                send('user_not_exist');
            }
            break;
        default:
            break;
    }
});
app.get('/akari/api/:requestType/', function(req, res){
    res.send('empty request data');
});

// App start.
app.listen(port, function(){
    console.log('Server started...');
});